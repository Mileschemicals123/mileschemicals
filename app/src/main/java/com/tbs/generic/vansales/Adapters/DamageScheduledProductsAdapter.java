package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.app.Dialog;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.ActiveDeliveryDO;
import com.tbs.generic.vansales.Model.LoadStockDO;
import com.tbs.generic.vansales.Model.ReasonDO;
import com.tbs.generic.vansales.Model.ReasonMainDO;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.Requests.DamageReasonsListRequest;
import com.tbs.generic.vansales.common.AppConstants;
import com.tbs.generic.vansales.listeners.LoadStockListener;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.Util;

import java.util.ArrayList;

public class DamageScheduledProductsAdapter extends RecyclerView.Adapter<DamageScheduledProductsAdapter.MyViewHolder>  {
    public ArrayList<ActiveDeliveryDO> loadStockDOS;
    private Context context;
    private ReasonMainDO reasonMainDo;
    private String imageURL;
    private double weight = 0;
    private double volume = 0;

    private LoadStockListener loadStockListener;
    double mass = 0.0;


    public void refreshAdapter(ArrayList<ActiveDeliveryDO> loadStockDoS) {
        this.loadStockDOS = loadStockDoS;
        notifyDataSetChanged();
    }

    public ArrayList<ActiveDeliveryDO> selectedLoadStockDOs = new ArrayList<>();
    private String from = "";
    private String damage = "";

    public ArrayList<ActiveDeliveryDO> getSelectedLoadStockDOs() {
        return selectedLoadStockDOs;
    }

    public DamageScheduledProductsAdapter(Context context, ArrayList<ActiveDeliveryDO> loadStockDoS, String from, String damagE) {
        this.context = context;
        this.loadStockDOS = loadStockDoS;
        this.from = from;
        this.damage = damagE;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.damage, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ActiveDeliveryDO loadStockDO = loadStockDOS.get(position);
        holder.tvProductName.setText(loadStockDO.product);
        holder.tvDescription.setText(loadStockDO.productDescription);
        holder.tvNumber.setText("Qty : " + loadStockDO.orderedQuantity+" " +loadStockDO.unit);
        if(loadStockDO.reason.length()>0){
            holder.tvSelection.setText("" + loadStockDO.reason);

        }else {
            holder.tvSelection.setHint(context.getString(R.string.select_reason));
        }

        int reasonId = ((BaseActivity) context).preferenceUtils.getIntFromPreference(PreferenceUtils.DAMAGE_REASON_ID, 0);
        if (reasonId > 0) {
            String reason = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.DAMAGE_REASON, "");

            holder.tvSelection.setText("" + reason);
        }
        holder.tvSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.llSelectReason.performClick();
            }
        });
        if (damage.equalsIgnoreCase("Damage")) {
            if (Util.isNetworkAvailable(context)) {
                DamageReasonsListRequest siteListRequest = new DamageReasonsListRequest(context);
                siteListRequest.setOnResultListener(new DamageReasonsListRequest.OnResultListener() {
                    @Override
                    public void onCompleted(boolean isError, ReasonMainDO reasonMainDO) {

                        ((BaseActivity) context).hideLoader();
                        if (reasonMainDO != null) {
                            if (isError) {
                                ((BaseActivity) context).showAppCompatAlert("", context.getString(R.string.no_reason_found), context.getString(R.string.ok), context.getString(R.string.cancel), "", false);
                            } else {

                                reasonMainDo = reasonMainDO;

                            }
                        } else {
                            ((BaseActivity) context).hideLoader();
                            ((BaseActivity) context).showAppCompatAlert("", context.getString(R.string.no_reason_found), context.getString(R.string.ok), context.getString(R.string.cancel), "", false);
                        }

                    }

                });
                siteListRequest.execute();
            } else {
                ((BaseActivity) context). showAppCompatAlert(context.getString(R.string.alert), context.getResources().getString(R.string.internet_connection), context.getString(R.string.ok), "", "",false);

            }


        }

        //need to get it from non-schedule product's qty
        holder.llSelectReason.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (damage.equalsIgnoreCase("Damage")) {

                    Dialog quantityDialog = new Dialog(context);
                    quantityDialog.setCancelable(true);
                    quantityDialog.setCanceledOnTouchOutside(true);
                    quantityDialog.setContentView(R.layout.simple_list);
                    ListView list = quantityDialog.findViewById(R.id.lvItems);
                    ReasonAdapter reasonAdapter = new ReasonAdapter();
                    if (reasonMainDo != null && reasonMainDo.reasonDOS.size() > 0) {
                        list.setAdapter(reasonAdapter);
                    } else {
                        ((BaseActivity) context).showToast(context.getString(R.string.no_reasons_found));
                    }
                    list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                            quantityDialog.dismiss();
                            holder.tvSelection.setText(reasonMainDo.reasonDOS.get(i).reason);
                            loadStockDO.reasonId=reasonMainDo.reasonDOS.get(i).reasonID;
                            loadStockDO.reason=reasonMainDo.reasonDOS.get(i).reason;

                        }
                    });
                    quantityDialog.show();
                }
            }
        });







    }

    @Override
    public int getItemCount() {
        return loadStockDOS != null ? loadStockDOS.size() : 0;
    }

    private int getCount() {

        int count = 0;
        for (LoadStockDO loadStockDO : AppConstants.listStockDO) {
            if (loadStockDO.quantity > 0) {
                count = count + 1;
            }
        }
        return count;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvProductName, tvDescription, tvAvailableQty, tvNumber, etTotalMeterQty, tvSelection;
        public RelativeLayout rlRemove, rlAdd;
        public ImageView ivRemove, ivAdd, ivDelete;
        public LinearLayout llAddRemove, llMeterReadings, llSelectReason;
        public EditText tvNumberET, etMeterReading1, etMeterReading2;

        private CheckBox cbSelected;

        public MyViewHolder(View view) {
            super(view);
            tvProductName    = view.findViewById(R.id.tvName);
            tvDescription    = view.findViewById(R.id.tvDescription);
            tvNumber         = view.findViewById(R.id.tvNumber);

            llAddRemove      = view.findViewById(R.id.llAddRemove);
            llSelectReason   = view.findViewById(R.id.llSelectReason);
            tvSelection      = view.findViewById(R.id.tvSelection);

        }
    }


    public void reasons(ArrayList<ReasonDO> reasonDoS) {

    }

    class ReasonAdapter extends BaseAdapter {
        private TextView tvQuantity;

        @Override
        public Object getItem(int i) {
            return i;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = inflater.inflate(R.layout.reason_text, null);
            tvQuantity = v.findViewById(R.id.tvQuantity);
            tvQuantity.setText(reasonMainDo.reasonDOS.get(i).reason);


            return v;
        }

        @Override
        public int getCount() {
            if (reasonMainDo.reasonDOS.size() > 0)
                return reasonMainDo.reasonDOS.size();
            else
                return 0;
        }

    }
}
