package com.tbs.generic.vansales.fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tbs.generic.vansales.Adapters.LoadStockAdapter
import com.tbs.generic.vansales.Model.LoadStockDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.NewLoadVanSaleRequest
import com.tbs.generic.vansales.Requests.NonScheduledLoadVanSaleRequest
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.fragment_open_stock.*
import java.util.*

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [OpenStockFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [OpenStockFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class OpenStockFragment : Fragment() {

    private lateinit var preferenceUtils: PreferenceUtils
    private var listener: OnFragmentInteractionListener? = null
    private var scheduleDos: ArrayList<LoadStockDO> = ArrayList()
    private var nonScheduleDos: ArrayList<LoadStockDO> = ArrayList()
    private var availableStockDos: ArrayList<LoadStockDO> = ArrayList()


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment OpenStockFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(): OpenStockFragment {
            val openStockFragment = OpenStockFragment()

            return openStockFragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_open_stock, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        preferenceUtils = PreferenceUtils(context)
        try {

            loadVehicleStockData()
        } catch (e: Exception) {
            e.localizedMessage
        }

        //Listners
        btn_vechical_stock.setOnClickListener({
            listener?.onFragmentVehicleStockInteraction()

        })
    }


    private fun loadVehicleStockData() {
        if (Util.isNetworkAvailable(activity)) {
            val scheduledRootId = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")
            if (!scheduledRootId.equals("", true)) {
                val loadVanSaleRequest = NewLoadVanSaleRequest(scheduledRootId, activity)
                Util.showLoader(prgress_bar)
                loadVanSaleRequest.setOnResultListener { isError, loadStockMainDo ->
                    if (!isError) {
                        //Util.showToast(activity, "No vehicle data found.")
                        scheduleDos = loadStockMainDo.loadStockDOS
                    }
                    loadNonScheduledVehicleStockData()
                }
                loadVanSaleRequest.execute()
            } else {
                loadNonScheduledVehicleStockData()
            }
        } else {
            Util.showToast(activity, "No Internet connection, please try again later")
        }
    }

    private fun loadNonScheduledVehicleStockData() {
        if (activity == null) {
            return
        }
        if (Util.isNetworkAvailable(activity)) {
            val nonScheduledRootId = preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "")
            if (!nonScheduledRootId.equals("", true)) {
                val loadVanSaleRequest = NonScheduledLoadVanSaleRequest(nonScheduledRootId, activity)
                Util.showLoader(prgress_bar)
                loadVanSaleRequest.setOnResultListener { isError, loadStockMainDo ->
                    Util.hideLoader(prgress_bar)
                    if (isError) {
                        //Util.showToast(activity, "No vehicle data found.")
                        showHideViews(false, getString(R.string.no_vehicle_data_found))
                    } else {
                        nonScheduleDos = loadStockMainDo.loadStockDOS
                        var isProductExisted = false
                        availableStockDos = nonScheduleDos
                        for (i in scheduleDos.indices) {
                            for (k in availableStockDos.indices) {
                                if (scheduleDos.get(i).product.equals(availableStockDos.get(k).product, true)) {
                                    availableStockDos.get(k).quantity = availableStockDos.get(k).quantity + scheduleDos.get(i).quantity
                                    isProductExisted = true
                                    break
                                }
                            }
                            if (isProductExisted) {
                                isProductExisted = false
                                continue
                            } else {
                                availableStockDos.add(scheduleDos.get(i))
                            }

                        }
                        if (availableStockDos.size > 0) {
                            showHideViews(true, "")
                            val loadStockAdapter = LoadStockAdapter(activity, availableStockDos, "Shipments")
                            recycleview.adapter = loadStockAdapter
//
                        } else {
                            showHideViews(false, getString(R.string.order_no_data))
                        }
                    }
                }
                loadVanSaleRequest.execute()
            } else {
//                tvNonScheduledStock.setText("0")
                val loadStockAdapter = LoadStockAdapter(activity, availableStockDos, "Shipments")
                recycleview.adapter = loadStockAdapter
            }
        } else {
            Util.showToast(activity, getString(R.string.no_internet))
        }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentVehicleStockInteraction()
    }

    private fun showHideViews(isSucess: Boolean, message: String) {
        try {
            if (isSucess) {
                no_data_found.visibility = View.GONE
                recycleview.visibility = View.VISIBLE
            } else {
                no_data_found.visibility = View.VISIBLE
                recycleview.visibility = View.GONE
                no_data_found.text = message
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }


    }


}
