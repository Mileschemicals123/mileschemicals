package com.tbs.generic.vansales.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class UserActivityReportMainDO implements Serializable {

    public Double totalAmount                        = 0.0;
    public String userName                           = "";
    public String transactionDate                    = "";
    public String site                               = "";
    public String currency                           = "";

    public ArrayList<UserDeliveryDO> userDeliveryDOS = new ArrayList<>();
    public ArrayList<MiscStopDO> miscStopDOS = new ArrayList<>();
    public ArrayList<PurchaseReturnDO> purchaseReturnDOS = new ArrayList<>();
    public ArrayList<PurchaseReceiptDO> purchaseReceiptDOS = new ArrayList<>();
    public ArrayList<CustomerReturnDO> customerReturnDOS = new ArrayList<>();
    public ArrayList<PickTicketDO> pickTicketDOS = new ArrayList<>();

    public ArrayList<SalesInvoiceDO> salesInvoiceDOS = new ArrayList<>();
    public ArrayList<CashDO> cashDOS                 = new ArrayList<>();
    public ArrayList<ChequeDO> chequeDOS             = new ArrayList<>();
    public ArrayList<LoadStockDO> productDOS         = new ArrayList<>();
    public ArrayList<LoadStockDO> stockDOS           = new ArrayList<>();
    public Double deliveryTotalQty                   = 0.0;
    public Double receiptTotalQty                   = 0.0;
    public Double purchaseReturnTotalQty                   = 0.0;
    public Double customerReturnTotalQty                   = 0.0;
    public Double pickTicketTotalQty                   = 0.0;

    public Double invoiceTotalQuantity               = 0.0;
    public Double invoiceTotalAmount                 = 0.0;
    public Double cashTotalAmount                    = 0.0;
    public Double chequeTotalAmount                  = 0.0;
    public Double productsTotalQty                   = 0.0;
    public Double stockTotalQty                      = 0.0;
    public int flag                                  = 0;
    public String message                            = "";
    public String companyCode                           = "";
    public String companyDescription                           = "";



}
