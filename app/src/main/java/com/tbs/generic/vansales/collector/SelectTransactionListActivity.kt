package com.tbs.generic.vansales.collector

import android.app.DatePickerDialog
import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import android.widget.*
import com.tbs.generic.vansales.Activitys.BaseActivity
import com.tbs.generic.vansales.Adapters.InvoiceAdapter
import com.tbs.generic.vansales.Model.LoadStockDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.PaymentTransactionListRequest
import com.tbs.generic.vansales.utils.PreferenceUtils
import java.text.SimpleDateFormat
import java.util.*


class SelectTransactionListActivity : BaseActivity() {
    lateinit var invoiceAdapter: InvoiceAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var tvStartDate: TextView
    lateinit var tvEndDate: TextView
    var fromId = 0
    var startDate = ""
    var endDate = ""
    var from = ""
    var to = ""
    private var cday: Int = 0
    private var cmonth: Int = 0
    private var cyear: Int = 0
    private lateinit var siteListRequest: PaymentTransactionListRequest

    override fun onResume() {
        super.onResume()
//        selectInvoiceList()
    }

    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.select_date, null) as LinearLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }

    override fun initializeControls() {
        val id = preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_DRIVER_NAME, "")
        tvScreenTitle.text = id
        val c = Calendar.getInstance()
        val cyear = c.get(Calendar.YEAR)
        val cmonth = c.get(Calendar.MONTH)
        val cday = c.get(Calendar.DAY_OF_MONTH)
        val btnCreate = findViewById<Button>(R.id.btnSelect)
        tvStartDate = findViewById<TextView>(R.id.tvStartDate)
        tvEndDate = findViewById<TextView>(R.id.tvEndDate)
//        datePicker.getDatePicker().setMinDate(System.currentTimeMillis()-1000);

        tvStartDate.setOnClickListener {
            val datePicker = DatePickerDialog(this@SelectTransactionListActivity, datePickerListener, cyear, cmonth, cday)

            datePicker.show()
        }

        tvEndDate.setOnClickListener {
            val datePicker = DatePickerDialog(this@SelectTransactionListActivity, endDatePickerListener, cyear, cmonth, cday)

            datePicker.show()
        }

        btnCreate.setOnClickListener {
            val sdf = SimpleDateFormat("yyyyMMdd")
            val date1 = sdf.parse(startDate)
            val date2 = sdf.parse(endDate)
            var start = tvStartDate.text.toString()
            var end = tvEndDate.text.toString()

            System.out.println("date1 : " + sdf.format(date1))
            System.out.println("date2 : " + sdf.format(date2))
            if (start.isEmpty()) {
                showToast("Select From Date")

            } else if (end.isEmpty()) {
                showToast("Select To Date")

            } else if (date1.compareTo(date2) > 0) {
                showToast("To date must be greater")
            } else if (date1.compareTo(date2) === 0) {
                showToast("From and To should not be same")
            } else {
                val intent = Intent(this@SelectTransactionListActivity, TransactionListActivity::class.java)
                intent.putExtra("START", startDate)
                intent.putExtra("END", endDate)
                intent.putExtra("FROM", from)
                intent.putExtra("TO", to)
                startActivityForResult(intent, 1)
            }

        }
    }

    private val datePickerListener: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        cday = dayOfMonth
        cmonth = monthOfYear + 1
        cyear = year
        var monthString = cmonth.toString()
        if (monthString.length == 1) {
            monthString = "0$monthString"
        } else {
            monthString = cmonth.toString()
        }

        var dayString = cday.toString()
        if (dayString.length == 1) {
            dayString = "0$dayString"
        } else {
            dayString = cday.toString()
        }
        startDate = "" + cyear + monthString + dayString
        tvStartDate.text = "" + dayString + " - " + monthString + " - " + cyear
        from = tvStartDate.text.toString()

    }
    private val endDatePickerListener: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        cday = dayOfMonth
        cmonth = monthOfYear + 1
        cyear = year
        var monthString = cmonth.toString()
        if (monthString.length == 1) {
            monthString = "0$monthString"
        } else {
            monthString = cmonth.toString()
        }

        var dayString = cday.toString()
        if (dayString.length == 1) {
            dayString = "0$dayString"
        } else {
            dayString = cday.toString()
        }
        endDate = "" + cyear + monthString + dayString
        tvEndDate.text = "" + dayString + " - " + monthString + " - " + cyear
        to = tvEndDate.text.toString()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == 1) {
        }
    }


}