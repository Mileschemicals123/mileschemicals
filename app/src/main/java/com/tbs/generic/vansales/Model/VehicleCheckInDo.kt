package com.tbs.generic.vansales.Model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 *Created by kishoreganji on 21-12-2018.
 *Copyright (C) 2018 TBS - All Rights Reserved
 */
data class VehicleCheckInDo(
        @SerializedName("id") var id: String? = "",
        @SerializedName("user_id") var user_id: String? = "",
        @SerializedName("confirmArrival") var confirmArrival: String? = "",
        @SerializedName("arrivalTime") var arrivalTime: String? = "",
        @SerializedName("startLoading") var startLoading: String? = "",
        @SerializedName("startLoadingTime") var startLoadingTime: String? = "",
        @SerializedName("scheduledRootId") var scheduledRootId: String? = "",
        @SerializedName("nonScheduledRootId") var nonScheduledRootId: String? = "",
        @SerializedName("loadStock") var loadStock: String? = "",
        @SerializedName("endLoading") var endLoading: String? = "",
        @SerializedName("endLoadTime") var endLoadTime: String? = "",
        @SerializedName("vehicleInspectionId") var vehicleInspectionId: String? = "",
        @SerializedName("gateInspectionTime") var gateInspectionTime: String? = "",
        @SerializedName("vehicleInspectionTime") var vehicleInspectionTime: String? = "",
        @SerializedName("gateInspectionId") var gateInspectionId: String? = "",
        @SerializedName("checkInStatus") var checkInStatus: String? = "",
        @SerializedName("checkInTime") var checkInTime: String? = "",
        @SerializedName("actualvehicleCheckinDate") var actualvehicleCheckinDate: String? = "",
        @SerializedName("actualvehicleCheckin") var actualvehicleCheckin: String? = "",
        @SerializedName("actualvehicleCheckout") var actualvehicleCheckout: String? = "",
        @SerializedName("statusflag") var statusflag: String? = "",

        @SerializedName("notes") var notes: String? = "",
//TimeCaptures for Checkin
        @SerializedName("checkinTimeCaptureArrivalTime") var checkinTimeCaptureArrivalTime: String? = "",
        @SerializedName("checkinTimeCaptureArrivalDate") var checkinTimeCaptureArrivalDate: String? = "",
        @SerializedName("checkinTimeCaptureStartLoadingTime") var checkinTimeCaptureStartLoadingTime: String? = "",
        @SerializedName("checkinTimeCaptureStartLoadingDate") var checkinTimeCaptureStartLoadingDate: String? = "",
        @SerializedName("checkOutTimeCaptureStartLoadingTime") var checkinTimeCaptureStockLoadingTime: String? = "",
        @SerializedName("checkOutTimeCaptureStartLoadingDate") var checkinTimeCaptureStockLoadingDate: String? = "",
        @SerializedName("checkinTimeCaptureEndLoadingTime") var checkinTimeCaptureEndLoadingTime: String? = "",
        @SerializedName("checkinTimeCaptureEndLoadingDate") var checkinTimeCaptureEndLoadingDate: String? = "",
        @SerializedName("checkinTimeCaptureVehicleInspectionTime") var checkinTimeCaptureVehicleInspectionTime: String? = "",
        @SerializedName("checkinTimeCapturVehicleInspectionDate") var checkinTimeCaptureVehicleInspectionDate: String? = "",
        @SerializedName("checkinTimeCaptureGateInspectionTime") var checkinTimeCaptureGateInspectionTime: String? = "",
        @SerializedName("checkinTimeCapturGateInspectionDate") var checkinTimeCaptureGateInspectionDate: String? = "",
        @SerializedName("checkinTimeCaptureCheckinTime") var checkinTimeCaptureCheckinTime: String? = "",
        @SerializedName("checkinTimeCapturCheckinDate") var checkinTimeCapturCheckinDate: String? = "",

        @SerializedName("checkinLattitude") var checkinLattitude: String? = "",
        @SerializedName("checkinLongitude") var checkinLongitude: String? = "",
        @SerializedName("checkoutLattitude") var checkoutLattitude: String? = "",
        @SerializedName("checkoutLongitude") var checkoutLongitude: String? = "",


        @SerializedName("confirmCheckOutArrival") var confirmCheckOutArrival: String? = "",
        @SerializedName("arrivalCheckOutTime") var arrivalCheckOutTime: String? = "",
        @SerializedName("startUnLoading") var startUnLoading: String? = "",
        @SerializedName("startUnLoadingTime") var startUnLoadingTime: String? = "",
        @SerializedName("unLoadStock") var unLoadStock: String? = "",
        @SerializedName("endUnLoading") var endUnLoading: String? = "",
        @SerializedName("endUnLoadTime") var endUnLoadTime: String? = "",
        @SerializedName("vehicleCheckOutInspectionId") var vehicleCheckOutInspectionId: String? = "",
        @SerializedName("gateCheckOutInspectionTime") var gateCheckOutInspectionTime: String? = "",
        @SerializedName("vehicleCheckOutInspectionTime") var vehicleCheckOutInspectionTime: String? = "",
        @SerializedName("gateCheckOutInspectionId") var gateCheckOutInspectionId: String? = "",
        @SerializedName("checkOutStatus") var checkOutStatus: String? = "",
        @SerializedName("checkOutTime") var checkOutTime: String? = "",

//Time Captures for Checkout
        @SerializedName("checkOutTimeCaptureArrivalTime") var checkOutTimeCaptureArrivalTime: String? = "",
        @SerializedName("checkOutTimeCaptureArrivalDate") var checkOutTimeCaptureArrivalDate: String? = "",
        @SerializedName("checkOutTimeCaptureStartLoadingTime") var checkOutTimeCaptureStartLoadingTime: String? = "",
        @SerializedName("checkOutTimeCaptureStartLoadingDate") var checkOutTimeCaptureStartLoadingDate: String? = "",
        @SerializedName("checkOutTimeCaptureStockLoadingTime") var checkOutTimeCaptureStockLoadingTime: String? = "",
        @SerializedName("checkOutTimeCaptureStockLoadingDate") var checkOutTimeCaptureStockLoadingDate: String? = "",
        @SerializedName("checkOutTimeCaptureEndLoadingTime") var checkOutTimeCaptureEndLoadingTime: String? = "",
        @SerializedName("checkOutTimeCaptureEndLoadingDate") var checkOutTimeCaptureEndLoadingDate: String? = "",
        @SerializedName("checkOutTimeCaptureVehicleInspectionTime") var checkOutTimeCaptureVehicleInspectionTime: String? = "",
        @SerializedName("checkOutTimeCapturVehicleInspectionDate") var checkOutTimeCapturVehicleInspectionDate: String? = "",
        @SerializedName("checkOutTimeCaptureGateInspectionTime") var checkOutTimeCaptureGateInspectionTime: String? = "",
        @SerializedName("checkOutTimeCapturGateInspectionDate") var checkOutTimeCapturGateInspectionDate: String? = "",
        @SerializedName("checkOutTimeCapturecheckOutTime") var checkOutTimeCapturecheckOutTime: String? = "",
        @SerializedName("checkOutTimeCapturcheckOutDate") var checkOutTimeCapturcheckOutDate: String? = ""

        ) : Serializable