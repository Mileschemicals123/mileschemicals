package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.FileDetails;
import com.tbs.generic.vansales.Model.PodDo;
import com.tbs.generic.vansales.Model.ValidateDeliveryDO;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.database.StorageManager;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class ValidateDeliveryRequest extends AsyncTask<String, Void, Boolean> {


    private Context mContext;
    private ValidateDeliveryDO validateDeliveryDO;
    private String id, notes, lattitude, longitude, address, signature;
    PreferenceUtils preferenceUtils;
    String message = "";
    ArrayList<String> images;

    public ValidateDeliveryRequest(ArrayList<String> images, String signature, String note, String id, String lattitudE, String longitudE, String addresS, Context mContext) {

        this.mContext = mContext;
        this.images = images;
        this.signature = signature;
        this.id = id;
        this.notes = note;
        this.lattitude = lattitudE;
        this.longitude = longitudE;
        this.address = addresS;
    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, ValidateDeliveryDO validateDeliveryDO, String msg);

    }

    public boolean runRequest() {
        preferenceUtils = new PreferenceUtils(mContext);
        String nonSheduledId = preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "");
        String SheduledCode = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "");
        String nonSheduledCode = preferenceUtils.getStringFromPreference(PreferenceUtils.NON_VEHICLE_CODE, "");
        String role = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "");
        String idd = preferenceUtils.getStringFromPreference(PreferenceUtils.EMIRATES_ID, "");
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("I_YSDHNUM", id);
            jsonObject.put("I_XSTKVCR", nonSheduledId);
            jsonObject.put("I_YCREUSR", role);
            jsonObject.put("I_YEMIRATEID", idd);
            jsonObject.put("I_YMSG", notes);
            jsonObject.put("I_YPRHNUM", "");
            jsonObject.put("I_XNOOFIMGES", images.size());

            jsonObject.put("I_YSIGNATURE", signature);
            jsonObject.put("I_XX10CLAT", lattitude);
            jsonObject.put("I_XX10CLOG", longitude);
            jsonObject.put("I_XX10CADD", address);
            if (SheduledCode.length() > 0) {

                jsonObject.put("I_YVEHCODE", SheduledCode);
            } else {

                jsonObject.put("I_YVEHCODE", nonSheduledCode);
            }


            JSONArray jsonArray = new JSONArray();
            if (images != null && images.size() > 0) {
                for (int i = 0; i < images.size(); i++) {
                    JSONObject jsonObject1 = new JSONObject();
                    jsonObject1.put("I_YIMAGE", images.get(i));

                    jsonArray.put(i, jsonObject1);
                }
                jsonObject.put("GRP2", jsonArray);
            }

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();
        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.VALIDATE_DELIVERY, jsonObject);
        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("validated xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();

            validateDeliveryDO = new ValidateDeliveryDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {
                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        //    createPaymentDO.customerDetailsDos = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        //      createPaymentDO = new CustomerDetailsDo();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {


                        if (attribute.equalsIgnoreCase("I_YSDHNUM")) {
                            validateDeliveryDO.shipmentId = text;


                        } else if (attribute.equalsIgnoreCase("O_YFLG")) {
                            validateDeliveryDO.flag = Integer.parseInt(text);

                        } else if (attribute.equalsIgnoreCase("O_YFLG2")) {
                            validateDeliveryDO.flag2 = Integer.parseInt(text);

                        } else if (attribute.equalsIgnoreCase("O_YSTATUS")) {
                            if (text.length() > 0) {

                                validateDeliveryDO.status = Integer.parseInt(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YMESSAGE")) {
                            if (text.length() > 0) {

                                validateDeliveryDO.message = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSDHPRT")) {
                            if (text.length() > 0) {

                                validateDeliveryDO.print = Integer.parseInt(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSDHEML")) {
                            if (text.length() > 0) {

                                validateDeliveryDO.email = Integer.parseInt(text);
                            }

                        }
                    }


                    if (endTag.equalsIgnoreCase("GRP")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ((BaseActivity) mContext).showLoader();

//         ProgressTask.getInstance().showProgress(mContext, false, "Validating the Delivery...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
//        ProgressTask.getInstance().closeProgress();

        if (onResultListener != null) {
            onResultListener.onCompleted(!result, validateDeliveryDO, ServiceURLS.message);
        }
        ((BaseActivity) mContext).hideLoader();

    }
}