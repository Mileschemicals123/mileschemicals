package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.SummaryDO;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;


public class SummaryRequest extends AsyncTask<String, Void, Boolean> {
    private Context mContext;
    String username, passWord, ip, pool, port,pwd;
    PreferenceUtils preferenceUtils;
    SummaryDO summaryDO ;
    public SummaryRequest(Context mContext) {

        this.mContext = mContext;
    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError,SummaryDO summaryDO);

    }

    public boolean runRequest() {

        preferenceUtils = new PreferenceUtils(mContext);
        String vr = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "");

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("I_XNUMPC", vr);

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();
        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.SUMMARY, jsonObject);
        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        try {
            String text = "", attribute = "", startTag = "", attribute2 = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();
            summaryDO = new SummaryDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    if (startTag != null && startTag.equalsIgnoreCase("FLD")) {

                         if (attribute.equalsIgnoreCase("O_XTOTDIS")) {
                            if (text.length() > 0) {
                                summaryDO.totalDistance = Double.valueOf(text);

                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_XTOTTRAVDIS")) {

                            summaryDO.totalTravelledDistance= Double.valueOf(text);

                        }
                         else if (attribute.equalsIgnoreCase("O_XTIMUN")) {

                             summaryDO.timeUNIT= text;

                         }
                         else if (attribute.equalsIgnoreCase("O_XDISUN")) {

                             summaryDO.distanceUNIT= text;

                         }
                         else if (attribute.equalsIgnoreCase("O_XOTIME")) {

                             summaryDO.onTIME= Double.valueOf(text);

                         }
                        else if (attribute.equalsIgnoreCase("O_XPERCANTAGE")) {

                            summaryDO.percentage= Double.valueOf(text);

                        }
                        else if (attribute.equalsIgnoreCase("O_XTOTTIM")) {

                            summaryDO.totalTime= Double.valueOf(text);

                        }
                        else if (attribute.equalsIgnoreCase("O_XTOTTRAVTIM")) {

                            summaryDO.totalTravelledTime= Double.valueOf(text);

                        }
                        else if (attribute.equalsIgnoreCase("O_XTOTSTOPS")) {

                            summaryDO.totalStops= Integer.parseInt(text);

                        }
                        else if (attribute.equalsIgnoreCase("O_XCOMSTOPS")) {
                             if(text.length()>0){
                                 summaryDO.completedStops= Integer.parseInt(text);
                             }

                        }
                        else if (attribute.equalsIgnoreCase("O_XTOTDROPS")) {

                            summaryDO.totalDrops= Integer.parseInt(text);

                        }
                        else if (attribute.equalsIgnoreCase("O_XCOMDROPS")) {

                            summaryDO.completedDrops= Integer.parseInt(text);

                        }
                        else if (attribute.equalsIgnoreCase("O_XTOTPICKUPS")) {

                            summaryDO.totalPickups= Integer.parseInt(text);

                        }
                        else if (attribute.equalsIgnoreCase("O_XCOMPICKUPS")) {
                           if(text.length()>0){
                               summaryDO.completedPickups= Integer.parseInt(text);

                           }

                        }
                         else if (attribute.equalsIgnoreCase("O_XMESSAGE")) {
                             if(text.length()>0){
                                 summaryDO.message= text;

                             }

                         }
                         else if (attribute.equalsIgnoreCase("O_XSTATUS")) {
                             if(text.length()>0){
                                 summaryDO.flag= Integer.parseInt(text);

                             }

                         }
                        text="";
                    }
                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }
                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception " + e);

            return false;
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
      //  ProgressTask.getInstance().showProgress(mContext, false, "Checking Credentials...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        if (onResultListener != null ) {
            if(summaryDO!=null){

                onResultListener.onCompleted(!result,summaryDO);
            }
            else {
                if (preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "") .isEmpty()) {
                    ((BaseActivity)mContext).showAppCompatAlert(mContext.getString(R.string.alert), mContext.getString(R.string.please_finish_config_first) , mContext.getString(R.string.ok), "", mContext.getString(R.string.failure), false);

                }else {

                    ((BaseActivity)mContext).showAppCompatAlert(mContext.getString(R.string.alert), mContext.getString(R.string.server_erro), mContext.getString(R.string.ok), "", mContext.getString(R.string.failure), false);
                }
            }
        }
    }
}