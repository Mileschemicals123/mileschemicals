package com.tbs.generic.vansales.DirectionActivitys;

import java.util.List;


public interface Parser {
    List<Route> parse() throws RouteException;
}