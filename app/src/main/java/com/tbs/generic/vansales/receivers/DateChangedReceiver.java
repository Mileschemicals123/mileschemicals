package com.tbs.generic.vansales.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.tbs.generic.vansales.common.FireBaseOperations;

public class DateChangedReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("DateChangedReceiver", "Date changed");
        FireBaseOperations.checkingDate(context);
    }


}