package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.itextpdf.text.Phrase;
import com.tbs.generic.vansales.Activitys.AddressListActivity;
import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Activitys.ScheduleNonScheduleActivity;
import com.tbs.generic.vansales.Model.CustomerDo;
import com.tbs.generic.vansales.Model.VRSelectionDO;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.common.AppConstants;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class VrSelectionAdapter extends RecyclerView.Adapter<VrSelectionAdapter.MyViewHolder> {

    private ArrayList<VRSelectionDO> vrSelectionDOS;
    private Context context;


    public void refreshAdapter(@NotNull ArrayList<VRSelectionDO> vrSelectionDoS) {
        this.vrSelectionDOS = vrSelectionDoS;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvVR, tvTRn, tvSite, tvDate, tvTripNumber;
        public LinearLayout llVR;

        public MyViewHolder(View view) {
            super(view);
            tvVR = itemView.findViewById(R.id.tvVR_ID);
            tvTRn = itemView.findViewById(R.id.tvTransactionID);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvSite = itemView.findViewById(R.id.tvSite);
            tvTripNumber = itemView.findViewById(R.id.tvTripNumber);
            llVR = itemView.findViewById(R.id.llVR);


        }
    }


    public VrSelectionAdapter(Context context, ArrayList<VRSelectionDO> vrSelectionDOS) {
        this.context = context;
        this.vrSelectionDOS = vrSelectionDOS;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.vr_selection_data, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final VRSelectionDO vrSelectionDO = vrSelectionDOS.get(position);
//        final VRSelectionDO vrSelectionDO = new VRSelectionDO();
        if ((position % 2 == 0)) {
            holder.itemView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.edit_text_background_lg));
        } else {
            holder.itemView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.edit_text_background_lsg));
        }
        holder.tvSite.setText(vrSelectionDO.siteID + " - " + vrSelectionDO.siteDescription);
        if (vrSelectionDO.scheduleDate != null && vrSelectionDO.scheduleDate.length() > 0) {
            String dMonth = vrSelectionDO.scheduleDate.substring(4, 6);
            String dyear = vrSelectionDO.scheduleDate.substring(0, 4);
            String dDate = vrSelectionDO.scheduleDate.substring(Math.max(vrSelectionDO.scheduleDate.length() - 2, 0));
            holder.tvDate.setText(dDate + "-" + dMonth + "-" + dyear);

        }
        holder.tvTRn.setText(vrSelectionDO.transactionID);
        if (!vrSelectionDO.vrID.isEmpty()) {
            holder.tvVR.setText(vrSelectionDO.vrID);
            holder.llVR.setVisibility(View.VISIBLE);
        } else {
            holder.llVR.setVisibility(View.GONE);

        }
        holder.tvTripNumber.setText("" + vrSelectionDO.trip);

        holder.itemView.setOnClickListener(view -> {
            if( !((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID,"").
                    equalsIgnoreCase(vrSelectionDO.vrID)){
                ((BaseActivity) context).preferenceUtils.removeFromPreference(PreferenceUtils.OPENING_READING);

            }
            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.VEHICLE_ROUTE_ID, "" + vrSelectionDO.vrID);
            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.Non_Scheduled_Route_Id, "" + vrSelectionDO.transactionID);
            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.DOC_NUMBER, vrSelectionDO.transactionID);
            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.VEHICLE_CODE, vrSelectionDO.vehicleCode);
            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.CARRIER_ID, vrSelectionDO.vehicleCarrier);
            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.LOCATION, vrSelectionDO.location);
            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.V_PLATE, vrSelectionDO.plate);
            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.N_SHIPMENTS, vrSelectionDO.nShipments);
            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.LOCATION_TYPE, vrSelectionDO.locationType);
            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.NON_VEHICLE_CODE, vrSelectionDO.vehicleCode);
            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.CA_TIME, vrSelectionDO.aTime);
            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.CA_DATE, vrSelectionDO.aDate);
            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.CD_DATE, vrSelectionDO.dDate);
            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.CD_TIME, vrSelectionDO.dTime);
            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.ODO_UNIT, vrSelectionDO.unit);
            ((BaseActivity) context).preferenceUtils.saveInt(PreferenceUtils.ODO_READ, vrSelectionDO.odometer);

            AppPrefs.putString(AppPrefs.CA_TIME,vrSelectionDO.aTime);
             AppPrefs.putString(AppPrefs.CA_DATE,vrSelectionDO.aDate);
            AppPrefs.putString(AppPrefs.CD_DATE,vrSelectionDO.dDate);
            AppPrefs.putString(AppPrefs.CD_TIME,vrSelectionDO.dTime);


            Log.d("eta-->",((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.CD_DATE, "") + "  " + ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.CD_TIME, "")+"--"+((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.CA_DATE, "") + "  " + ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.CA_TIME, ""));


           /* Intent intent = new Intent(context, ScheduleNonScheduleActivity.class);
            intent.putExtra(Constants.SCREEN_TYPE, context.getString(R.string.confirm_stock));
            context.startActivity(intent);*/
            ((BaseActivity) context).finish();
        });


    }

    @Override
    public int getItemCount() {
        return vrSelectionDOS != null ? vrSelectionDOS.size() : 10;
//        return 10;

    }

}
