package com.tbs.generic.vansales.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class NonScheduledDriverIdMainDO implements Serializable {

    public String driverName = "";
    public String notes          = "";

    public ArrayList<DriverDO> driverDOS = new ArrayList<>();

    public ArrayList<NonScheduledProductDO> nonScheduledProductDOS = new ArrayList<>();

}
