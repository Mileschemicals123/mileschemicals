package com.tbs.generic.vansales.Model;

import java.io.Serializable;

public class SummaryDO implements Serializable {


    public Double totalDistance = 0.0;
    public Double totalTravelledDistance = 0.0;
    public Double totalTime = 0.0;
    public Double totalTravelledTime = 0.0;
    public Double percentage = 0.0;
    public String timeUNIT = "";
    public String distanceUNIT = "";
    public Double onTIME = 0.0;

    public int totalStops = 0;
    public int completedStops = 0;
    public int totalDrops = 0;
    public int completedDrops = 0;
    public int totalPickups = 0;
    public int completedPickups = 0;
    public String message = "";
    public int flag = 0;


}
