package com.tbs.generic.vansales.Activitys

import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.ScrollView
import com.tbs.generic.vansales.R

//
class RegisterEventsActivity : BaseActivity() {

    override fun onResume() {
        super.onResume()
    }
    override fun initialize() {
      var  llCategories = layoutInflater.inflate(R.layout.fragment_register_events, null) as ScrollView
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        initializeControls()
        hideKeyBoard(llCategories)

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }

    }
    override fun initializeControls() {
        tvScreenTitle.setText(R.string.events)
        val btnCompleted = findViewById<Button>(R.id.btnCompleted)

        btnCompleted.setOnClickListener {
            finish()
        }
    }

}