package com.tbs.generic.pod.Activitys

import android.content.Intent
import android.view.View
import android.view.ViewGroup
import com.tbs.generic.vansales.Activitys.BaseActivity
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.database.StorageManager
import android.widget.*
import com.tbs.generic.vansales.Activitys.NewLoadVanSaleStockTabActivity


class StartDayActivity : BaseActivity() {


    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.start_day_layout, null) as ScrollView
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        disableMenuWithBackButton()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            finish()
//            overridePendingTransition(R.anim.exit, R.anim.enter)
        }
        initializeControls()


    }


    override fun initializeControls() {

        tvScreenTitle.text = "Start Day"
        var llCheckIn = findViewById<View>(R.id.llCheckIn) as LinearLayout
        var llLoadStock = findViewById<View>(R.id.llLoadStock) as LinearLayout
        llCheckIn.setOnClickListener {
//            val intent = Intent(this@StartDayActivity, CheckinScreen::class.java)
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//            startActivity(intent)
        }

        llLoadStock.setOnClickListener {
            val vehicleCheckInDo = StorageManager.getInstance(this@StartDayActivity).getVehicleCheckInData(this@StartDayActivity)
            if (!vehicleCheckInDo.checkInStatus!!.isEmpty()) {
                if (!vehicleCheckInDo.loadStock.equals("")) {
                    val intent = Intent(this@StartDayActivity, NewLoadVanSaleStockTabActivity::class.java)
                    intent.putExtra("FLAG", 1)
                    intent.putExtra("ScreenTitle", "Load Van Sales Stock")
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                    startActivity(intent)
                } else {
                    showToast("Please complete the stock load in check in screen")
                }
            } else {

                if (!vehicleCheckInDo.loadStock.equals("")) {
                    val intent = Intent(this@StartDayActivity, NewLoadVanSaleStockTabActivity::class.java)
                    intent.putExtra("FLAG", 1)
                    intent.putExtra("ScreenTitle", "Load Van Sales Stock")
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                    startActivity(intent)
                } else {
                    showAppCompatAlert("", "Please Finish Check-in Process", "OK", "", "", false)
                }

            }
        }





    }

    override fun onResume() {
        super.onResume()
    }


}