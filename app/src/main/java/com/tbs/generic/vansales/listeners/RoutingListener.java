//package com.tbs.generic.generic.listeners;
//
//import java.util.List;
//
//import okhttp3.Route;
//import okhttp3.internal.connection.RouteException;
//
//public interface RoutingListener {
//    void onRoutingFailure(RouteException e);
//
//    void onRoutingStart();
//
//    void onRoutingSuccess(List<Route> route, int shortestRouteIndex);
//
//    void onRoutingCancelled();
//}
