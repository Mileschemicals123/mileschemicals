package com.tbs.generic.vansales.Model;

import java.io.Serializable;

public class PriceDetailsDO implements Serializable {

    public String customerDescription = "";
    public String productDescription = "";
    public String currency = "";
    public String salesUnit = "";
    public String minQuantity = "";
    public String maxQuantity = "";
    public String price = "";
    public String customerName = "";
    public String productName = "";

}
