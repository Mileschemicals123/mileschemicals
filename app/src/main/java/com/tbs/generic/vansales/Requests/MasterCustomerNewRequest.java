package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.CustomerDo;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class MasterCustomerNewRequest extends AsyncTask<String, Void, Boolean> {

    ArrayList<CustomerDo> customers;

    private Context mContext;
    private String id;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;

    public MasterCustomerNewRequest(Context mContext) {

        this.mContext = mContext;
    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, ArrayList<CustomerDo> customers);

    }

    public boolean runRequest() {

        preferenceUtils = new PreferenceUtils(mContext);
        String id = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "");
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("I_YBPC", id);

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();
        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.MASTER_DATA, jsonObject);
        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }


    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();
            CustomerDo customer = null;
            customers = new ArrayList<>();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

//
                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        customer = new CustomerDo();
                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();
                    if (startTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("O_YBPCNUM")) {
                            if (text.length() > 0) {
                                customer.customer = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YBPCSHO")) {
                            if (text.length() > 0) {

                                customer.customerName = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YBPADDLIG")) {
                            if (text.length() > 0) {

                                customer.postBox = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YBPADDLIG1")) {
                            if (text.length() > 0) {

                                customer.landmark = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YBPADDLIG2")) {
                            if (text.length() > 0) {

                                customer.town = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YCRYNAME")) {
                            if (text.length() > 0) {

                                customer.countryName = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YCITY")) {
                            if (text.length() > 0) {

                                customer.city = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YWEB")) {
                            if (text.length() > 0) {

                                customer.webSite = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YPOS")) {
                            if (text.length() > 0) {

                                customer.postalCode = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YLD")) {
                            if (text.length() > 0) {

                                customer.landline = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YEM")) {
                            if (text.length() > 0) {

                                customer.email = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YFAX")) {
                            if (text.length() > 0) {

                                customer.fax = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YMOB")) {
                            if (text.length() > 0) {

                                customer.mobile = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YLAT")) {
                            if (text.length() > 0) {

                                customer.lattitude = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_LON")) {
                            if (text.length() > 0) {

                                customer.longitude = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YBPCRAMT")) {
                            if (text.length() > 0) {

                                customer.amount = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YBUS")) {
                            if (text.length() > 0) {

                                customer.businessLine = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YSAT")) {
                            if (text.length() > 0) {

                                customer.emirates = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YBPFLG")) {
                            if (text.length() > 0) {

                                customer.flag = Integer.parseInt(text);
//                                if(customer.flag==0){
//                                    customer.isCreditAvailable=true;
//                                }else {
//                                    customer.isCreditAvailable=false;
//
//                                }
                            }
                        }
                        text = "";


                    }
                    if (endTag.equalsIgnoreCase("LIN")) {
                        customers.add(customer);
                    }
                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();


                }
                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception " + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ((BaseActivity) mContext).showLoader();

        // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        ((BaseActivity) mContext).hideLoader();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, customers);
        }
    }
}